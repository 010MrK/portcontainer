package model;

public class Macara {
    private int timpManipulare;
    private enum tipContainer {Mic_10mc, Mediu_25mc, Mare_50mc, Jumbo_100mc}

    public Macara(int timpManipulare) {
        this.timpManipulare = timpManipulare;
    }

    public int getTimpManipulare() {
        return this.timpManipulare;
    }

    public void setTimpManipulare(int timpManipulare) {
        this.timpManipulare = timpManipulare;
    }

    interface Descarcare {
        abstract int DescarcaContainer(PortContainer portContainer, Macara macara);
    }
}
