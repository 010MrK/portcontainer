package model;

import java.util.Arrays;

public class PortContainer {
    private String eticheta;
    private enum tipContainer {Mic_10mc, Mediu_25mc, Mare_50mc, Jumbo_100mc}
    private int[] nrContainere = new int[4];

    public PortContainer(String eticheta, int[] nrContainere) {
        this.eticheta = eticheta;
        this.nrContainere = nrContainere;
    }

    public String getEticheta() {
        return eticheta;
    }

    public void setEticheta(String eticheta) {
        this.eticheta = eticheta;
    }

    public int[] getNrContainere() {
        return nrContainere;
    }

    public void setNrContainere(int[] nrContainere) {
        this.nrContainere = nrContainere;
    }

    interface Cloneable {

    }
    interface Numarabil {
        abstract int getCapacitate(tipContainer tipContainer);
    }

    @Override
    public String toString() {
        return "Nume clasa: " + getClass() + "\tValoare eticheta: " + getEticheta() + "\tNr.containere: " + Arrays.toString(getNrContainere());
    }
}
