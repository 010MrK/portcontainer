package com.company;

import model.PortContainer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
        PortContainer[] flotaPortContainere = new PortContainer[3];
        flotaPortContainere[0] = new PortContainer("001", new int[]{0, 1, 2, 3});
        flotaPortContainere[1] = new PortContainer("002", new int[]{0, 0, 0, 1});
        flotaPortContainere[2] = new PortContainer("003", new int[]{0, 2, 2, 0});

        //afisare date
        System.out.println("Flota 1 - eticheta: " + flotaPortContainere[0].getEticheta() +
                " / nr containere: " + Arrays.toString(flotaPortContainere[0].getNrContainere()));
        System.out.println("Flota 2 - eticheta: " + flotaPortContainere[1].getEticheta() +
                " / nr containere: " + Arrays.toString(flotaPortContainere[1].getNrContainere()));
        System.out.println("Flota 3 - eticheta: " + flotaPortContainere[2].getEticheta() +
                " / nr containere: " + Arrays.toString(flotaPortContainere[2].getNrContainere()));

        //scriere in fisier
        FileWriter csvWriter = new FileWriter("new.csv");

        for (PortContainer portContainer : flotaPortContainere) {
            csvWriter.append(portContainer.getEticheta());
            csvWriter.append(",");
            csvWriter.append(Arrays.toString(portContainer.getNrContainere()));
            csvWriter.append(",");
            csvWriter.append("\n");
        }
        csvWriter.flush();
        csvWriter.close();
    }

}
